import java.util.Scanner;

public class Assignment2
{
	public static void main(String[] args) 
		{
	
		//Enter 5 inputs to get the sum of 5 numbers

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the first input");
		int n1 = keyboard.nextInt();
		System.out.println("Please enter the second input");
		int n2 = keyboard.nextInt();
		System.out.println("Please enter the third input");
		int n3 = keyboard.nextInt();
		System.out.println("Please enter the fourth input");
		int n4 = keyboard.nextInt();
		System.out.println("Please enter the fifth input");
		int n5 = keyboard.nextInt();

		//calculating the sum of 5 numbers by adding them 
		int sum = n1+n2+n3+n4+n5;

		//calculating the average by deviding the sum by 5
		double average = sum/5;

		//calculating max and min for extra credit
		int max = Math.max(Math.max(Math.max(n1,n2), Math.max(n3,n4)),n5);
		int min = Math.min(Math.min(Math.min(n1,n2), Math.min(n3,n4)),n5);

		System.out.println("Sum = " + sum);
		System.out.println("Average = " + average);
		System.out.println("Max= " + max);
		System.out.println("Min= " + min);
	}
		
  }

